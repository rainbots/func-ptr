#=====[ NERDTree view after cleaning and building. ]=====
    # NERDTree view of build/ folder is refreshed. Check this with ';nr'.
    # If other folders need to be refreshed, use ';nR'.
#=====[ Makefile: refresher on the build tools ]=====
    # 'make' is a declarative language, i.e., tell 'make' your intention rather
    # than attempting to tell 'make' exactly how to do things.
    #
    # Use variables for lib names.
    # The lib name only appears once in the Makefile.
    #
    # Use pre-requisites.
    # Pre-requisites tell 'make' how to determine if a target is up to date.
    #
    #---Functions---
    # https://www.gnu.org/software/make/manual/html_node/Syntax-of-Functions.html#Syntax-of-Functions
    # ---Automatic Variables---
        # $^ -- all the prerequisites separated by spaces
        # $@ -- the target name
    # ---Pattern Rules---
    #Explicit rule for TARGETS (make it implicit by dropping 'TARGETS:')
        #TARGETS: TARGET-PATTERN: PREREQ-PATTERNS
        #    RECIPE
    # See explicit rules:
    #=====[ Explicit Rule for unity-extension-objects ]=====
    #=====[ Explicit Rule for lib-objects ]=====
test-path := test/
build-path := build/
unity-path := test/unity/
lib-path := lib/

#----------------------------------------------------------------------
# | This is the most important line in the whole file. Add libs here. |
#----------------------------------------------------------------------
lib-names := bsort IntegerList StringList
# Add more names space-separated like this:
#lib-names := libx liby libz
# Create the paths to the compiled lib objects.
lib-objects := $(addsuffix .o,${lib-names})
lib-objects := $(addprefix ${build-path},${lib-objects})
# Create the paths to the lib code.
lib-cfiles := $(addsuffix .c,${lib-names})
lib-cfiles := $(addprefix ${lib-path},${lib-cfiles})
lib-hfiles := $(addsuffix .h,${lib-names})
lib-hfiles := $(addprefix ${lib-path},${lib-hfiles})
# Create the paths to the lib unit test code.
lib-tests := $(addsuffix .c,${lib-names})
lib-tests := $(addprefix ${lib-path}test/test_,${lib-tests})
# Create the paths to the lib unit test objects.
lib-test-objects := $(addsuffix .o,${lib-names})
lib-test-objects := $(addprefix ${build-path}test_,${lib-test-objects})

CFLAGS = -g -Wall -Wextra #-pedantic
#	-g adds symbols for debugging

test-results: ${build-path}TestSuite-results.md

${build-path}TestSuite-results.md: ${build-path}TestSuite.exe
	./$^ > $@ 2> ${build-path}test-results_stderr.md
#	./$^ > $@

${build-path}TestSuite.exe: ${test-path}test_runner.c  ${build-path}unity.o ${lib-test-objects} ${lib-objects}
	${compiler} $(CFLAGS) $^ -o $@ \
		-I ${unity-path} \
		-I ${lib-path} \
		-I lib/test/

#=====[ Explicit rule for lib-test-objects ]=====
${lib-test-objects}: ${build-path}%.o: ${lib-path}test/%.c
	${compiler} $(CFLAGS) -c $^ -o $@ \
	-I ${unity-path} \
	-I ${lib-path}

#=====[ Explicit Rule for lib-objects ]=====
    # lib-objects are the items in lib-names with a .o suffix added.
    # The rule for lib-objects is:
        # The stem is the lib-name. The target is a .o.
        # Name the target with prefix 'build/'.
        # The prerequisite is the stem with a .c suffix,
        # and those .c files are in 'lib/'.
${lib-objects}: ${build-path}%.o: ${lib-path}%.c
	${compiler} $(CFLAGS) -c $^ -o $@ \
		-I ${lib-path}
        #-I ${lib-path} adds 'lib/' to the search path for #include with <>

${build-path}unity.o: ${unity-path}unity.c
	${compiler} $(CFLAGS) -c $^ -o $@
#-I ${unity-path}
    # ---gcc -I---
    # -I is to add a search directory for #include files.
    #        -I ${lib-path}
    # The -I option is not necessary here because libx.c includes libx.h and
    # libx.h is in the same folder as libx.c.
    # If libx.h was in a different folder, then we would need this -I and
    # instead of lib-path, we would include header-path.


#;mc
.PHONY: clean-all-builds
clean-all-builds:
	rm -f ${build-path}TestSuite-results.md
	rm -f ${build-path}test-results_stderr.md
	rm -f ${build-path}TestSuite.exe
	rm -f ${lib-objects}
	rm -f ${lib-test-objects}
	rm -f ${build-path}unity.o

