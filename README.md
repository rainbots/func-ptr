# Table of Contents
1. [Function pointers continued](#markdown-header-function-pointers-continued)
1. [Bubble Sort Kata](#markdown-header-bubble-sort-kata)

---e-n-d---

# Function pointers
## Function pointers continued

I went on a big [function pointers learning journey][func-ptr].
[func-ptr]: https://bitbucket.org/rainbots/test-doubles/src/master/

And now I finally get to try out these ideas on *Bubble Sort*. The idea is to
make a *Bubble Sort* that works like the `stdlib.h` function `qsort`:

```c
void qsort(
    void *base,     // THE ARRAY TO SORT (the address of its initial element)
    size_t nel,     // number of elements               in the array to sort
    size_t width,   // size in bytes of each object     in the array to sort
    int (*compar)(const void *, const void *) // how to compare two elements
    );
```

The first parameter of `qsort` is a *void-pointer*. This generalizes `qsort` to
sort an array of any datatype.

The last parameter of `qsort` is a *function pointer* typedef. Whatever function
name is passed into this argument, `compar` will alias to that function.

The client passes a *comparison* function telling `qsort` how to compare two
elements. The *comparison* function returns an `int`. The sign of the `int`
tells `qsort` whether the elements are in ascending order. If the `int` is
negative, they are in ascending order; if it is positive, descending order.

The *comparison* function takes two void-pointers. The void-pointers point to
the elements being compared. They are `const` because the elements shall not
change, only their order in the array changes.

## Bubble Sort Kata

[Barry Brown has a YouTube video][barry-brown] about *void pointers* and *function
pointers*, using bubble sort as an example. So I needed to set up a simple
bubble sort implementation to have some code that could be refactored into
something cleaner and more generalized using void pointers and function
pointers.
[barry-brown]: https://youtu.be/TpIuj3Lgho0?t=18m51s

---

Tasks:

    [x] Write a bubble sort for a list of integers.
        See ./lib/bsort.c:BubbleSortIntegers()
    [x] Write a bubble sort for a list of strings.
        See ./lib/bsort.c:BubbleSortStrings()

=====[ bsort.c: 18 ]=====
```c
void BubbleSortIntegers(int *nums, int len)
{
    for (int i=0; i<len; i++)
    {
        for (int i=0; i<(len-1); i++)
        {
            if (nums[i+1] < nums[i])
            {
                _swap(
                    &nums[i+1],
                    &nums[i]
                    );
            }
        }
    }
}
```
=====[ bsort.c: 35 ]=====
```c
void BubbleSortStrings(const char **word_list, int len)
{
    for (int i=0; i<(len); i++)
    {
        for (int i=0; i<(len-1); i++)
        {
            if (strcasecmp(word_list[i+1], word_list[i]) < 0)
            {
                _swap_strings(
                    &word_list[i+1],
                    &word_list[i]
                    );
            }
        }
    }
}
```

The algorithm is simple: for each element in the array, compare the element with
the next element. If it is not in order, swap the two.

This translates to an outer loop that looks at each index in the array, and an
inner loop that compares neighboring indices (which is why it has to stop
iterating at the pen-ultimate elemtent).

Notice that the *Integer* and *String* versions are almost identical. The
important differences are:

* the `base` datatype (borrowing the name from the `qsort`
parameter)
* the comparison function

The integer array is an array of `int`s: `int *nums`.
The string array is an array of words: `const char **word_list`

C has no strings, so a string is an array of `char`. This can be expressed as an
array, `char[]`, or as a pointer, `char *`. Function parameters always use the
pointer notation. In general, there are subtle but important differences between
the two notations, but function parameters are the one time when there is no
difference between the two forms. I guess the pointer form is considered
easier-to-read or more function-like since it's the more general of the two
forms.

Reading `char *` as *array of characters*, i.e., a *string*, then `char **`
reads as an *array of strings*, i.e., a *list of words*.

The comparison function for `int` is simple, just the `>` symbol.

For *strings*, I used `strcasecmp` from`strings.h`. It compares two strings by
reading the characters from left to right. If the characters between the two
strings do not match, it returns a number indicating the magnitude of the
difference in the ASCII codes. A positive number means the first string comes
after the second string alphabetically and should be swapped; a negative number
means the two strings are already correctly alphabetized. Uppercase/lowercase is
ignored.

---

Now use a void pointer to write a single bubble sort that can do integers or
strings. Barry improvises a useful but failed attempt at this [starting at this
point in the video][void-ptr-start] and [ending at this point][func-ptr-start],
where he starts showing the motivation for using function pointers.
[void-ptr-start]: https://youtu.be/TpIuj3Lgho0?t=18m51s
[func-ptr-start]: https://youtu.be/TpIuj3Lgho0?t=34m59s

TLDR: Here is the motivation for using a function pointer.

`BubbleSortIntegers` and `BubbleSortStrings` specify the datatype in the
parameter list. The compiler uses the automatic memory management model,
allocating stack memory to hold the arguments passed in by the caller. When the
function exits, this memory is automatically deallocated, hence the name.

The compiler knows how much memory to allocate in the stack frame because the
size of the datatype is known.

For example, the integer bubble sort takes a pointer to an `int`:

    =====[ lib/bsort.c:18 ]=====
    ```c
    void BubbleSortIntegers(int *nums, int len)
    ```

And the string bubble sort takes a pointer to an array of `const char`, i.e., a
pointer to a string:

    =====[ lib/bsort.c:35 ]=====
    ```c
    void BubbleSortStrings(const char **word_list, int len)
    ```

It is when these functions derefence the pointer to compare and to swap elements
that knowing what datatype the pointer points to becomes critical for the
compiler to know how much memory to allocate in the stack frame.

For example, the integer sorter:

    =====[ lib/bsort.c:35 ]=====
    ```c
    if (nums[i+1] < nums[i])
    {
        _swap(
            &nums[i+1],
            &nums[i]
            );
    }
    ```

Indexing array `nums` in the comparison is equivalent to dereferencing the
pointer (`int * nums`).
