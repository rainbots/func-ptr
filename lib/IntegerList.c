#include <stdio.h>  // printf
#include "IntegerList.h"

static void print_num_array(int *num_array, int array_len)
{   //Helper to manually confirm the sort by printing it before the test result.
    printf("---");
    for (int i=0; i<(array_len-1); i++) printf("%d, ", num_array[i]);
    printf(
        "%d---\n",
        num_array[(array_len-1)]
        );
}

//=====[ IntegerList: stuff that goes together ]=====
    //  Usage:
    //  ------
    //  Initialize a `list`:
    //      IntegerList_s list = MakeIntegerList();
    //  Print struct member `unsorted`:
    //      if (list.can_print) list.print(list, "Before Sorting: ");
    //  Sort struct member `unsorted`:
    //      BubbleSortIntegers(list.unsorted, list.len);
    //

static void PrintIntegerList(  // assign list.print to this function
    struct IntegerList_s list,
    char const *prefix
    )
{   // prefix is a message like "After Sorting:" to print before the list.
    printf("%s ", prefix);
    print_num_array(list.unsorted, list.len);
}

static int compare_ints(const void *a, const void *b)
{
    if (*(int *)a > *(int*)b) return 1;
    else return 0;
}

IntegerList_s MakeIntegerList(void)
{
    IntegerList_s list = {
        .element_size=sizeof(int),
        .compare=compare_ints,
        .unsorted={4, 3, 1, 2, 6, 7},
        .sorted={1, 2, 3, 4, 6, 7},
        .len=MaxListLength,
        .can_print=true,
        .print=PrintIntegerList
        };
    return list;
}

