#ifndef _INTEGERLIST_H
#define _INTEGERLIST_H

#include <stdbool.h>

#define MaxListLength 6
typedef struct IntegerList_s
{
    size_t element_size;
    int (*compare)(const void *, const void *);
    int unsorted[MaxListLength];
    int sorted[MaxListLength];
    int len;
    bool can_print;
    void (*print)(struct IntegerList_s, char const *);
}IntegerList_s;

IntegerList_s MakeIntegerList(void);

#endif // _INTEGERLIST_H
