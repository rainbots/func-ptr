#include "StringList.h"
#include <stdio.h>

static void print_word_list(const char ** word_list, int list_len)
{
    printf("---");
    for (int i=0; i<(list_len-1); i++) printf("%s, ", word_list[i]);
    printf(
        "%s---\n",
        word_list[(list_len-1)]
        );
}

//=====[ StringList: stuff that goes together ]=====
    //  Usage:
    //  ------
    //  Initialize a `list`:
    //      StringList_s list = MakeStringList();
    //  Print struct member `unsorted`:
    //      if (list.can_print) list.print(list, "Before Sorting: ");
    //  Sort struct member `unsorted`:
    //      BubbleSortString(list.unsorted, list.len);
    //

void PrintStringList(StringList_s list, string prefix)
{   // prefix is a message like "After Sorting:" to print before the list.
    printf("%s ", prefix);
    print_word_list(list.unsorted, list.len);
}

static int compare_strings(const void *a, const void *b)
{
    return strcasecmp(*(string*)a, *(string*)b);
}

StringList_s MakeStringList(void)
{
    StringList_s list = {
        .element_size=sizeof(string),  // passing pointers, not arrays!
        .compare=compare_strings,
        .unsorted={
            "retreat",
            "coat",
            "ream",
            "poach",
            "smile"
            },
        .sorted={
            "coat",
            "poach",
            "ream",
            "retreat",
            "smile"
            },
        .len=MaxStringListLength,
        .can_print=true,
        .print=PrintStringList
        };
    return list;
}


