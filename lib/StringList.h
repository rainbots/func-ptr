#ifndef _STRINGLIST_H
#define _STRINGLIST_H

#include <strings.h>  // size_t
#include <stdbool.h>

#define MaxStringListLength 5
typedef char const *string;
typedef struct StringList_s
{
    size_t element_size;
    int (*compare)(const void *, const void *);
    string unsorted[MaxStringListLength];
    string sorted[MaxStringListLength];
    int len;
    bool can_print;
    void (*print)(struct StringList_s, string);
} StringList_s;

StringList_s MakeStringList(void);

#endif // _STRINGLIST_H
