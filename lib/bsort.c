#include "bsort.h"
#include <strings.h>
#include <stdlib.h>

static void _swap(int *a, int *b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}

static void _swap_strings(const char **a, const char **b)
{
    const char *temp = *a;
    *a = *b;
    *b = temp;
}

void BubbleSortIntegers(int *nums, int len)
{
    for (int i=0; i<len; i++) {
        for (int i=0; i<(len-1); i++) {
            if (nums[i+1] < nums[i]) {
                _swap(
                    &nums[i+1],
                    &nums[i]
                );
            } } }
}

void BubbleSortStrings(const char **word_list, int len)
{
    for (int i=0; i<(len); i++) {
        for (int i=0; i<(len-1); i++) {
            if (strcasecmp(word_list[i+1], word_list[i]) < 0) {
                _swap_strings(
                    &word_list[i+1],
                    &word_list[i]
                );
            } } }
}
