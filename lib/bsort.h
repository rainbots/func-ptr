#ifndef _BSORT_H
#define _BSORT_H

void BubbleSortIntegers(int *numbers_to_sort, int array_length);
void BubbleSortStrings(const char **word_list_to_alphabetize, int list_length);

#endif // _BSORT_H
