#include <unity.h>
#include "test_bsort.h"
#include "bsort.h"
#include <stdbool.h>

//=====[ List of tests to write ]=====
    //  [x] BubbleSortIntegers should sort numbers in ascending order
    //  [x] BubbleSortStrings should sort strings in alphabetical order
    //  [ ] BubbleSort should sort numbers in ascending order
    //  [ ] BubbleSort should sort strings in alphabetical order

//TODO: pass a pointer-to-struct instead of the struct

void bsort_int_setUp(void){}
void bsort_int_tearDown(void){}

void print_num_array(int *num_array, int array_len)
{   //Helper to manually confirm the sort by printing it before the test result.
    printf("---");
    for (int i=0; i<(array_len-1); i++) printf("%d, ", num_array[i]);
    printf(
        "%d---\n",
        num_array[(array_len-1)]
        );
}

#define MaxListLength 6
typedef struct IntegerList_s
{
    int unsorted[MaxListLength];
    int sorted[MaxListLength];
    int len;
    bool can_print;
    void (*print)(struct IntegerList_s, char *);
}IntegerList_s;

static void PrintIntegerList(
    struct IntegerList_s list,
    char *prefix
    )
{   // prefix is a message like "After Sorting: " to print before the list.
    printf("%s ", prefix);
    print_num_array(list.unsorted, list.len);
}

static IntegerList_s MakeIntegerList(void)
{
    IntegerList_s list = {
        .unsorted={4, 3, 1, 2, 6, 7},
        .sorted={1, 2, 3, 4, 6, 7},
        .len=MaxListLength,
        .can_print=true,
        .print = PrintIntegerList
        };
    return list;
}


void test_bsort_BubbleSortIntegers_sorts_numbers_in_ascending_order(void)
{
    //=====[ Setup ]=====
    IntegerList_s list = MakeIntegerList();
    // list.can_print = false;  // do not print list.unsorted before each test
    //=====[ Operate ]=====
    if (list.can_print) list.print(list, "Before Sorting:");
    BubbleSortIntegers(list.unsorted, list.len);
    if (list.can_print) list.print(list, " After Sorting:");
    TEST_ASSERT_EQUAL_INT_ARRAY( // check that unsorted was correctly sorted
        list.sorted, list.unsorted, list.len
        );
}

