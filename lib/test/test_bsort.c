#include <unity.h>
#include "test_bsort.h"
#include "bsort.h"
#include <stdbool.h>
#include <stdlib.h>  // qsort
#include <IntegerList.h>
#include <StringList.h>

//=====[ List of tests to write ]=====
    //  [x] BubbleSortIntegers should sort numbers in ascending order
    //  [x] BubbleSortStrings should sort strings in alphabetical order
    //  [x] BubbleSort should sort numbers in ascending order
    //  [x] BubbleSort should sort strings in alphabetical order

void bsort_int_setUp(void){}
void bsort_int_tearDown(void){}

void test_bsort_BubbleSortIntegers_sorts_numbers_in_ascending_order(void)
{
    //=====[ Setup ]=====
    IntegerList_s list = MakeIntegerList(); list.can_print=false;
    //=====[ Operate ]=====
    if (list.can_print) list.print(list, "Before Sorting: ");
    BubbleSortIntegers(list.unsorted, list.len);
    if (list.can_print) list.print(list, " After Sorting: ");
    TEST_ASSERT_EQUAL_INT_ARRAY(
        list.sorted, list.unsorted, list.len
        );
}

void test_bsort_BubbleSortStrings_alphabetizes_a_passphrase(void)
{
    //=====[ Setup ]=====
    StringList_s list = MakeStringList();
    //=====[ Operate ]=====
    if (list.can_print) list.print(list, "Before Sorting:");
    BubbleSortStrings(list.unsorted, list.len);
    if (list.can_print) list.print(list, " After Sorting:");
    TEST_ASSERT_EQUAL_STRING_ARRAY(
        list.sorted,
        list.unsorted,
        list.len
        );
}

void test_bsort_BubbleSort_sorts_numbers_in_ascending_order(void)
{
    //=====[ Setup ]=====
    IntegerList_s list = MakeIntegerList(); list.can_print=false;
    //=====[ Operate ]=====
    if (list.can_print) list.print(list, "Before Sorting: ");
    qsort(list.unsorted, list.len, list.element_size, list.compare);
    if (list.can_print) list.print(list, " After Sorting: ");
    //=====[ Test ]=====
    TEST_ASSERT_EQUAL_INT_ARRAY(
        list.sorted, list.unsorted, list.len
        );
    //=====[ Setup2 ]===== try swapping two numbers and resorting
    int temp = list.unsorted[2];
    list.unsorted[2] = list.unsorted[3];
    list.unsorted[3] = temp;
    //=====[ Operate2 ]=====
    if (list.can_print) list.print(list, "After swap, before sorting: ");
    qsort(list.unsorted, list.len, list.element_size, list.compare);
    if (list.can_print) list.print(list, "After swapping and sorting: ");
    //=====[ Test2 ]=====
    TEST_ASSERT_EQUAL_INT_ARRAY(
        list.sorted, list.unsorted, list.len
        );
}

void test_bsort_BubbleSort_alphabetizes_a_passphrase(void)
{
    //=====[ Setup ]=====
    StringList_s list = MakeStringList();
    //=====[ Operate ]=====
    if (list.can_print) list.print(list, "Before Sorting:");
    qsort(list.unsorted, list.len, list.element_size, list.compare);
    if (list.can_print) list.print(list, " After Sorting:");
    TEST_ASSERT_EQUAL_STRING_ARRAY(
        list.sorted,
        list.unsorted,
        list.len
        );
}
