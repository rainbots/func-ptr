#ifndef _TEST_BSORT_H
#define _TEST_BSORT_H

void bsort_int_setUp(void); void bsort_int_tearDown(void);
void test_bsort_BubbleSortIntegers_sorts_numbers_in_ascending_order(void);
void test_bsort_BubbleSortStrings_alphabetizes_a_passphrase(void);
void test_bsort_BubbleSort_sorts_numbers_in_ascending_order(void);
void test_bsort_BubbleSort_alphabetizes_a_passphrase(void);

#endif // _TEST_BSORT_H
