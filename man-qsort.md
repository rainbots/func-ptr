QSORT(3P)                                                                        POSIX Programmer's Manual                                                                       QSORT(3P)

PROLOG
       This  manual  page  is  part of the POSIX Programmer's Manual.  The Cygwin implementation of this interface may differ (consult the corresponding Cygwin manual page for details of
       Cygwin behavior), or the interface may not be implemented on Cygwin.

NAME
       qsort — sort a table of data

SYNOPSIS
       #include <stdlib.h>

       void qsort(void *base, size_t nel, size_t width,
           int (*compar)(const void *, const void *));

DESCRIPTION
       The functionality described on this reference page is aligned with the
       ISO C standard. Any conflict between the requirements described here and
       the  ISO C  standard  is  uninten‐ tional. This volume of POSIX.1‐2008
       defers to the ISO C standard.

       The  qsort()  function  shall sort an array of nel objects, the initial
       element of which is pointed to by base.  The size of each object, in
       bytes, is specified by the width argu‐ ment. If the nel argument has the
       value zero, the comparison function pointed to by compar shall not be
       called and no rearrangement shall take place.

       The application shall ensure that the comparison function pointed to by
       compar does not alter the contents of the array. The implementation  may
       reorder  elements  of  the  array between calls to the comparison
       function, but shall not alter the contents of any individual element.

       When  the same objects (consisting of width bytes, irrespective of their
       current positions in the array) are passed more than once to the
       comparison function, the results shall be consistent with one another.
       That is, they shall define a total ordering on the array.

       The contents of the array shall be sorted in ascending order according to
       a comparison function. The compar argument is a pointer to the comparison
       function, which is called  with two  arguments  that  point  to the
       elements being compared. The application shall ensure that the function
       returns an integer less than, equal to, or greater than 0, if the first
       argument is considered respectively less than, equal to, or greater than
       the second. If two members compare as equal, their order in the sorted
       array is unspecified.

RETURN VALUE
       The qsort() function shall not return a value.

ERRORS
       No errors are defined.

       The following sections are informative.

EXAMPLES
       None.

APPLICATION USAGE
       The comparison function need not compare every byte, so arbitrary data may be contained in the elements in addition to the values being compared.

RATIONALE
       The requirement that each argument (hereafter referred to as p) to the comparison function is a pointer to elements of the array implies that for every  call,  for  each  argument
       separately, all of the following expressions are non-zero:

           ((char *)p − (char *)base) % width == 0
           (char *)p >= (char *)base
           (char *)p < (char *)base + nel * width

FUTURE DIRECTIONS
       None.

SEE ALSO
       alphasort()

       The Base Definitions volume of POSIX.1‐2008, <stdlib.h>

COPYRIGHT
       Portions  of  this text are reprinted and reproduced in electronic form from IEEE Std 1003.1, 2013 Edition, Standard for Information Technology -- Portable Operating System Inter‐
       face (POSIX), The Open Group Base Specifications Issue 7, Copyright (C) 2013 by the Institute of  Electrical  and  Electronics  Engineers,  Inc  and  The  Open  Group.   (This  is
       POSIX.1-2008  with  the 2013 Technical Corrigendum 1 applied.) In the event of any discrepancy between this version and the original IEEE and The Open Group Standard, the original
       IEEE and The Open Group Standard is the referee document. The original Standard can be obtained online at http://www.unix.org/online.html .

       Any typographical or formatting errors that appear in this page are most likely to have been introduced during the conversion of the source files to man  page  format.  To  report
       such errors, see https://www.kernel.org/doc/man-pages/reporting_bugs.html .

IEEE/The Open Group                                                                        2013                                                                                  QSORT(3P)
