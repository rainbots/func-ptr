#include <stdio.h>
#include <unity.h>
#include <test_bsort.h>

int main(void)
{
    UNITY_BEGIN();
    setUp = bsort_int_setUp; tearDown = bsort_int_tearDown;
    RUN_TEST(test_bsort_BubbleSortIntegers_sorts_numbers_in_ascending_order);
    return UNITY_END();
}

