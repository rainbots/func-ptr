#include <stdio.h>
#include <unity.h>
#include <test_bsort.h>

void (*setUp   )(void);
void (*tearDown)(void);

int main(void)
{
    UNITY_BEGIN();
    setUp = bsort_int_setUp; tearDown = bsort_int_tearDown;
    RUN_TEST(test_bsort_BubbleSortIntegers_sorts_numbers_in_ascending_order);
    RUN_TEST(test_bsort_BubbleSort_sorts_numbers_in_ascending_order);
    RUN_TEST(test_bsort_BubbleSortStrings_alphabetizes_a_passphrase);
    RUN_TEST(test_bsort_BubbleSort_alphabetizes_a_passphrase);
    return UNITY_END();
}
